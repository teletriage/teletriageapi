<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAction extends Model
{
    
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'title', 'is_active',
   ];
}
