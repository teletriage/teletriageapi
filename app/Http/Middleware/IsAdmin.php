<?php

namespace App\Http\Middleware;

use App\Util\Constants;
use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user()->hasRole('Admin')){
            return response()->json(['error'=>'No autorizado'], Constants::FORBIDDEN_STATUS);
        }
        return $next($request);
    }
}
