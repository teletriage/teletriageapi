<?php

namespace App\Http\Controllers;

use App\Company;
use App\Municipality;
use App\Specialty;
use App\Util\Constants;
use Illuminate\Http\Request;

class MunicipalityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;

        $municipalities = Municipality::where("name","LIKE","%{$request->input('query')}%")
                                        ->orderBy('name', 'asc')
                                        ->paginate($limit);

        $municipalities->appends(array(
            'limit' => $limit
        ));

        return response()->json($this->transformCollection($municipalities), Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $municipality = Municipality::find($id);
        if(!$municipality){
            return response()->json(['error' => ['message' => 'Municipio no encontrado']], Constants::NOT_FOUND_STATUS);
        }
        return response()->json(['data' => $this->transformSingle($municipality)], Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $municipality
     * @return array
     */
    private function transformList($municipality){
        return [
            'id' => $municipality['id'],
            'name' => $municipality['name'],
            'code' => $municipality['code']
        ];
    }

    /**
     * @param $municipality
     * @return array
     */
    private function transformSingle($municipality){
        return [
            'id' => $municipality['id'],
            'name' => $municipality['name'],
            'code' => $municipality['code']
        ];
    }

    /**
     * @param $ageranges
     * @return array
     */
    private function transformCollection($municipalities){
        $municipalitiesArray = $municipalities->toArray();
        return [
            'total' => $municipalitiesArray['total'],
            'per_page' => intval($municipalitiesArray['per_page']),
            'current_page' => $municipalitiesArray['current_page'],
            'last_page' => $municipalitiesArray['last_page'],
            'next_page_url' => $municipalitiesArray['next_page_url'],
            'prev_page_url' => $municipalitiesArray['prev_page_url'],
            'from' => $municipalitiesArray['from'],
            'to' => $municipalitiesArray['to'],
            'data' => array_map([$this, 'transformList'], $municipalitiesArray['data'])
        ];
    }
}
