<?php

namespace App\Http\Controllers;

use App\ResultAction;
use App\Util\Constants;
use Illuminate\Http\Request;

class ResultActionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;

        $resultActions = ResultAction::where('is_active', 1)->paginate($limit);

        $resultActions->appends(array(
            'limit' => $limit
        ));

        return response()->json($this->transformCollection($resultActions), Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $resultAction = ResultAction::find($id);
        if(!$resultAction){
            return response()->json(['error' => ['message' => 'Resultado no encontrada']], Constants::NOT_FOUND_STATUS);
        }
        return response()->json(['data' => $this->transformSingle($resultAction)], Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $resultAction
     * @return array
     */
    private function transformList($resultAction){
        return [
            'id' => $resultAction['id'],
            'text' => $resultAction['text'],
            'level' => $resultAction['level'],
            'is_active' => $resultAction['is_active']
        ];
    }

    /**
     * @param $resultAction
     * @return array
     */
    private function transformSingle($resultAction){
        return [
            'id' => $resultAction['id'],
            'text' => $resultAction['text'],
            'level' => $resultAction['level'],
            'is_active' => $resultAction['is_active']
        ];
    }

    /**
     * @param $categories
     * @return array
     */
    private function transformCollection($resultActions){
        $resultActionsArray = $resultActions->toArray();
        return [
            'total' => $resultActionsArray['total'],
            'per_page' => intval($resultActionsArray['per_page']),
            'current_page' => $resultActionsArray['current_page'],
            'last_page' => $resultActionsArray['last_page'],
            'next_page_url' => $resultActionsArray['next_page_url'],
            'prev_page_url' => $resultActionsArray['prev_page_url'],
            'from' => $resultActionsArray['from'],
            'to' => $resultActionsArray['to'],
            'data' => array_map([$this, 'transformList'], $resultActionsArray['data'])
        ];
    }
}
