<?php

namespace App\Http\Controllers;

use App\UserLog;
use Illuminate\Http\Request;
use App\Util\Constants;

class UserLogController extends Controller
{

   /*  public function __construct(){
        $this->middleware([Constants::IS_ADMIN_MIDD])->only('index', 'store');
        $this->middleware([Constants::RESOURCE_MOD_MIDD])->only('update', 'destroy');
    } */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userId = $request->input('user_id') ? $request->input('user_id') : 0;
        return UserLog::where('user_id', $userId)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userLog = new UserLog;

        $userLog->user_id = $request->user_id;
        $userLog->user_action_id = $request->user_action_id;

        $userLog->save();

        return response()->json([
            'message' => 'Successfully stored user log'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return UserLog::where('id', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
