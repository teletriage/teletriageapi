<?php

namespace App\Http\Controllers;

use App\Question;
use App\User;
use App\Util\Constants;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware([Constants::IS_ADMIN_MIDD])->only('index', 'store');
        $this->middleware([Constants::RESOURCE_MOD_MIDD])->only('update', 'destroy');
    }

    /**
     * Metodo para listar todos los usuarios del sistema
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;

        $users = User::with('roles')->paginate($limit);

        $users->appends(array(
            'limit' => $limit
        ));

        return response()->json($this->transformCollection($users), Constants::SUCCESS_STATUS);
    }

    /**
     * Metodo para crear usuario
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'              => 'required|string',
            'email'             => 'required|string|email|unique:users',
            'password'          => 'required|string|confirmed',
            'last_name'         => 'required|string',
            'document_type_id'  => 'required|integer',
            'document'          => 'required|string',
            'medical_licence'   => 'required|string',
            'company_id'        => 'required|integer',
            'speciality_id'     => 'required|integer',

        ]);
        $user = new User([
            'name'              => $request->name,
            'email'             => $request->email,
            'password'          => bcrypt($request->password),
            'last_name'         => $request->last_name,
            'document_type_id'  => $request->document_type_id,
            'document'          => $request->document,
            'medical_licence'   => $request->medical_licence,
            'company_id'        => $request->company_id,
            'speciality_id'     => $request->speciality_id,
        ]);
        $user->save();

        //TODO implementar el campo de roles para crear usuarios desde la administracion
        return response()->json(['message' => 'Usuario creado con éxito!'], Constants::CREATED_STATUS);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $user = Question::with('roles')->find($id);
        if(!$user){
            return response()->json(['error' => ['message' => 'Usuario no encontrado']], Constants::NOT_FOUND_STATUS);
        }
        return response()->json(['data' => $this->transformSingle($user)], Constants::SUCCESS_STATUS);
    }

    /**
     * @param $user
     * @return array
     */
    private function transformList($user){
        return [
            'id' => $user['id'],
            'name' => $user['name'],
            'email' => $user['email'],
            'roles' => array_map([$this, 'transformRoles'], $user['roles'])
        ];
    }

    /**
     * @param $question
     * @return array
     */
    private function transformSingle($user){
        return [
            'id' => $user['id'],
            'name' => $user['name'],
            'email' => $user['email'],
            'roles' => array_map([$this, 'transformRoles'], $user['roles']->toArray())
        ];
    }

    /**
     * @param $role
     * @return array
     */
    private function transformRoles($role){
        return [
            'id' => $role['id'],
            'name' => $role['name']
        ];
    }

    /**
     * @param $users
     * @return array
     */
    private function transformCollection($users){
        $usersArray = $users->toArray();
        return [
            'total' => $usersArray['total'],
            'per_page' => intval($usersArray['per_page']),
            'current_page' => $usersArray['current_page'],
            'last_page' => $usersArray['last_page'],
            'next_page_url' => $usersArray['next_page_url'],
            'prev_page_url' => $usersArray['prev_page_url'],
            'from' => $usersArray['from'],
            'to' => $usersArray['to'],
            'data' => array_map([$this, 'transformList'], $usersArray['data'])
        ];
    }
}
