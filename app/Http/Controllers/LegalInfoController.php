<?php

namespace App\Http\Controllers;

use App\LegalInfo;
use Illuminate\Http\Request;
use App\Util\Constants;

class LegalInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $legal_info = LegalInfo::where('is_active', 1)->first();
        return response()->json($legal_info, Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id'   => 'required',
            'text'      => 'required|string',
        ]);

        $legalInfo = new LegalInfo([
            'user_id'   => $request->user_id,
            'text'      => $request->text,
            'is_active' => true,
        ]);


        $legalInfo->save();

        //TODO implementar el campo de roles para crear usuarios desde la administracion
        return response()->json(['message' => 'Información Legal almacenada con éxito!'], Constants::CREATED_STATUS);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
