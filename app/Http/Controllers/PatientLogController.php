<?php

namespace App\Http\Controllers;

use App\PatientLog;
use App\UserLog;
use Illuminate\Http\Request;
use App\Util\Constants;

class PatientLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $resultAction = $request->input('resultAction');
        $user = $request->input('user_id');
        $beginDate = $request->input('beginDate');
        $endDate = $request->input('endDate');
        $locality = $request->input('locality');
        $gender = $request->input('gender');
        $municipality = $request->input('municipality');
        $patientLogs = PatientLog::with(
            'user',
            'municipality',
            'ageRange',
            'company',
            'resultAction'
        )->when($resultAction, function ($query, $resultAction) {
            return $query->where('result_action_id', $resultAction);
        })->when($user, function ($query, $user) {
            return $query->where('user_id', $user);
        })->when($beginDate, function ($query, $beginDate) {
            return $query->where('created_at','>=', $beginDate);
        })->when($endDate, function ($query, $endDate) {
            return $query->where('created_at','<=', $endDate);
        })->when($locality, function ($query, $locality) {
            return $query->where('locality_id', $locality);
        })->when($gender, function ($query, $gender) {
            return $query->where('gender', $gender);
        })->when($municipality, function ($query, $municipality) {
            return $query->where('municipality_id', $municipality);
        })
        ->paginate($limit);
        $patientLogs->appends(array(
            'limit' => $limit
        ));
        return response()->json($this->transformCollection($patientLogs), Constants::SUCCESS_STATUS); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id'              => 'required',
            'municipality_id'      => 'required',
            'age_range_id'         => 'required',
            'age'                  => 'required|Integer',
            'symptoms_origin_date' => 'required|date',
            'gender'               => 'required|Integer',
            'company_id'           => 'required',
            'result_action_id'     => 'required',

        ]);

        $patientLog = new PatientLog([
            'user_id'              => $request->user_id,
            'municipality_id'      => $request->municipality_id,
            'locality_id'          => $request->locality_id,
            'age_range_id'         => $request->age_range_id,
            'age'                  => $request->age,
            'symptoms_origin_date' => $request->symptoms_origin_date, 
            'gender'               => $request->gender,
            'company_id'           => $request->company_id,
            'result_action_id'     => $request->result_action_id,
        ]);
        $patientLog->save();

        //TODO implementar el campo de roles para crear usuarios desde la administracion
        return response()->json([
            'message' => 'Log de Diagnostico almacenado con éxito!',
            'PatientLogId' => $patientLog->id
        ], Constants::CREATED_STATUS);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patientLog = PatientLog::find($id);
        if(!$patientLog){
            return response()->json(['error' => ['message' => 'Paciente no encontrado']], Constants::NOT_FOUND_STATUS);
        }
        return response()->json(['data' => $this->transformSingle($patientLog)], Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $patientLog
     * @return array
     */
    private function transformList($patientLog)
    {
        return [
            'id' => $patientLog['id'],
            'user' => $this->transformUser($patientLog['user']),
            'municipality' => $this->transformMunicipality($patientLog['municipality']),
            'locality_id' => $patientLog['locality_id'],
            'age_range' => $this->transformAgeRange($patientLog['age_range']),
            'gender' => $patientLog['gender'],
            'company' => $this->transformCompany($patientLog['company']),
            'result_action' => $this->transformResultAction($patientLog['result_action']),
            'created_at' => $patientLog['created_at'],
        ];
    }

    /**
     * @param $patientLog
     * @return array
     */
    private function transformSingle($patientLog)
    {
        return [
            //'patientLog' => $patientLog,
            'id' => $patientLog['id'],
            'user' => $this->transformUser($patientLog['user']),
            'municipality' => $this->transformMunicipality($patientLog['municipality']),
            'locality_id' => $patientLog['locality_id'],
            'age_range' => $this->transformAgeRange($patientLog['ageRange']),
            'gender' => $patientLog['gender'],
            'company' => $this->transformCompany($patientLog['company']),
            'result_action' => $this->transformResultAction($patientLog['resultAction']),
            'created_at' => $patientLog['created_at'],
        ];
    }

    /**
     * @param $patientLogs
     * @return array
     */
    private function transformCollection($patientLogs)
    {
        $patientLogsArray = $patientLogs->toArray();
        return [
            'total' => $patientLogsArray['total'],
            'per_page' => intval($patientLogsArray['per_page']),
            'current_page' => $patientLogsArray['current_page'],
            'last_page' => $patientLogsArray['last_page'],
            'next_page_url' => $patientLogsArray['next_page_url'],
            'prev_page_url' => $patientLogsArray['prev_page_url'],
            'from' => $patientLogsArray['from'],
            'to' => $patientLogsArray['to'],
            'data' => array_map([$this, 'transformList'], $patientLogsArray['data'])
        ];
    }

    private function transformResultAction($resultAction){
        return [
            'id' => $resultAction['id'],
            'text' => $resultAction['text'],
            'level' => $resultAction['level'],
            'is_active' => $resultAction['is_active']
        ];
    }

    /**
     * @param $range
     * @return array
     */
    private function transformAgeRange($range){
        return [
            'id' => $range['id'],
            'title' => $range['title'],
            'description' => $range['description'],
            'is_active' => $range['is_active']
        ];
    }

    /**
     * @param $range
     * @return array
     */
    private function transformMunicipality($municipality){
        return [
            'id' => $municipality['id'],
            'code' => $municipality['code'],
            'name' => $municipality['name'],
        ];
    }

    /**
     * @param $range
     * @return array
     */
    private function transformCompany($company){
        return [
            'id' => $company['id'],
            'name' => $company['name'],
            'entity_code' => $company['entity_code'],
            'entity_type' => $company['entity_type'],
        ];
    }

    private function transformUser($user){
        return [
            'id' => $user['id'],
            'name' => $user['name'],
            'last_name' => $user['last_name'],
            'medical_licence' => $user['medical_licence'],
            'company_id' => $user['company_id'],
        ];
    }
}
