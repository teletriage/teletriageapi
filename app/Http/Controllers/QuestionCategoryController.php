<?php

namespace App\Http\Controllers;

use App\QuestionCategory;
use App\Util\Constants;
use Illuminate\Http\Request;

class QuestionCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;

        $categories = QuestionCategory::where('is_active', 1)->paginate($limit);

        $categories->appends(array(
            'limit' => $limit
        ));

        return response()->json($this->transformCollection($categories), Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $category = QuestionCategory::find($id);
        if(!$category){
            return response()->json(['error' => ['message' => 'Cateogria no encontrada']], Constants::NOT_FOUND_STATUS);
        }
        return response()->json(['data' => $this->transformSingle($category)], Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $category
     * @return array
     */
    private function transformList($category){
        return [
            'id' => $category['id'],
            'description' => $category['description'],
            'title' => $category['title'],
            'is_active' => $category['is_active']
        ];
    }

    /**
     * @param $category
     * @return array
     */
    private function transformSingle($category){
        return [
            'id' => $category['id'],
            'description' => $category['description'],
            'title' => $category['title'],
            'is_active' => $category['is_active']
        ];
    }

    /**
     * @param $categories
     * @return array
     */
    private function transformCollection($categories){
        $categoriesArray = $categories->toArray();
        return [
            'total' => $categoriesArray['total'],
            'per_page' => intval($categoriesArray['per_page']),
            'current_page' => $categoriesArray['current_page'],
            'last_page' => $categoriesArray['last_page'],
            'next_page_url' => $categoriesArray['next_page_url'],
            'prev_page_url' => $categoriesArray['prev_page_url'],
            'from' => $categoriesArray['from'],
            'to' => $categoriesArray['to'],
            'data' => array_map([$this, 'transformList'], $categoriesArray['data'])
        ];
    }
}
