<?php

namespace App\Http\Controllers;

use App\Locality;
use App\Util\Constants;
use Illuminate\Http\Request;

class LocalityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;

        $localities = Locality::orderBy('name', 'asc')
                                ->paginate($limit);

        $localities->appends(array(
            'limit' => $limit
        ));

        return response()->json($this->transformCollection($localities), Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $locality = Locality::find($id);
        if(!$locality){
            return response()->json(['error' => ['message' => 'Localidad no encontrada']], Constants::NOT_FOUND_STATUS);
        }
        return response()->json(['data' => $this->transformSingle($locality)], Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $locality
     * @return array
     */
    private function transformList($locality){
        return [
            'id' => $locality['id'],
            'name' => $locality['name'],
            'code' => $locality['code']
        ];
    }

    /**
     * @param $locality
     * @return array
     */
    private function transformSingle($locality){
        return [
            'id' => $locality['id'],
            'name' => $locality['name'],
            'code' => $locality['code']
        ];
    }

    /**
     * @param $ageranges
     * @return array
     */
    private function transformCollection($localities){
        $localitiesArray = $localities->toArray();
        return [
            'total' => $localitiesArray['total'],
            'per_page' => intval($localitiesArray['per_page']),
            'current_page' => $localitiesArray['current_page'],
            'last_page' => $localitiesArray['last_page'],
            'next_page_url' => $localitiesArray['next_page_url'],
            'prev_page_url' => $localitiesArray['prev_page_url'],
            'from' => $localitiesArray['from'],
            'to' => $localitiesArray['to'],
            'data' => array_map([$this, 'transformList'], $localitiesArray['data'])
        ];
    }
}
