<?php

namespace App\Http\Controllers;

use App\Util\Constants;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller {

    public function __construct(){
        $this->middleware([Constants::IS_ADMIN_MIDD]);
    }

    /**
     * Metodo para mostrar todos roles
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function role_list()
    {
        $roles = Role::all();
        return response()->json(['roles' => $roles], Constants::SUCCESS_STATUS);
    }

    /**
     * Meotdo para almacenar nuevo rol
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function role_store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:roles'
        ]);
        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], Constants::UNAUTHORIZED_STATUS);
        }
        $input = $request->all();
        $role = Role::create(['name' => $input['name']]);
        if($role){
            return response()->json(['role' => $role],  Constants::SUCCESS_STATUS);
        }
        return response()->json(['error' => "No se puede crear un rol"], Constants::UNAUTHORIZED_STATUS);
    }

    /**
     * Metodo para listar todos los permisos
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function permission_list()
    {
        $permissions = Permission::all();
        return response()->json(['permissions' => $permissions], Constants::SUCCESS_STATUS);
    }

    /**
     * Metodo para crear un nuevo permiso
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function permission_store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if($validator->fails()){
            return response()->json(['error' => $validator->errors()], Constants::UNAUTHORIZED_STATUS);
        }
        $input = $request->all();
        $permission = Permission::create(['name' => $input['name']]);
        if($permission){
            return response()->json(['permission' => $permission], Constants::SUCCESS_STATUS);
        }
    }

    /**
     * Metodo para asignar un permiso a un rol
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function role_has_permissions(Request $request){
        $validator = Validator::make($request->all(), [
            'role_id' => 'required|exists:roles,id',
            'permission_id' => 'required|exists:permissions,id'
        ]);
        if($validator->fails()){
            return response()->json(['error' => $validator->errors()], Constants::UNAUTHORIZED_STATUS);
        }
        $role = Role::where('id', $request['role_id'])->first();
        $permission = Permission::where('id', $request['permission_id'])->first();
        if($role->givePermissionTo($permission)){
            return response()->json(['success' => $role], Constants::SUCCESS_STATUS);
        }
    }

    /**
     * Metodo para asignar un usuario a un rol
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assign_user_to_role(Request $request){
        $validator = Validator::make($request->all(), [
            'role_id' => 'required|exists:roles,id',
            'user_id' => 'required|exists:users,id'
        ]);
        if($validator->fails()){
            return response()->json(['error' => $validator->errors()], Constants::UNAUTHORIZED_STATUS);
        }
        $role = Role::where('id', $request['role_id'])->first();
        $user = User::where('id', $request['user_id'])->first();
        if($user->assignRole($role)){
            return response()->json(['success' => $user], Constants::SUCCESS_STATUS);
        }
    }
}
