<?php

namespace App\Http\Controllers;

use App\AgeRange;
use App\Util\Constants;
use Illuminate\Http\Request;

class AgeRangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;

        $ageranges = AgeRange::where('is_active', 1)->paginate($limit);

        $ageranges->appends(array(
            'limit' => $limit
        ));

        return response()->json($this->transformCollection($ageranges), Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $agerange = AgeRange::find($id);
        if(!$agerange){
            return response()->json(['error' => ['message' => 'Rango de edad no encontrado']], Constants::NOT_FOUND_STATUS);
        }
        return response()->json(['data' => $this->transformSingle($agerange)], Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $agerange
     * @return array
     */
    private function transformList($agerange){
        return [
            'id' => $agerange['id'],
            'description' => $agerange['description'],
            'title' => $agerange['title'],
            'is_active' => $agerange['is_active']
        ];
    }

    /**
     * @param $agerange
     * @return array
     */
    private function transformSingle($agerange){
        return [
            'id' => $agerange['id'],
            'description' => $agerange['description'],
            'title' => $agerange['title'],
            'is_active' => $agerange['is_active']
        ];
    }

    /**
     * @param $ageranges
     * @return array
     */
    private function transformCollection($ageranges){
        $agerangesArray = $ageranges->toArray();
        return [
            'total' => $agerangesArray['total'],
            'per_page' => intval($agerangesArray['per_page']),
            'current_page' => $agerangesArray['current_page'],
            'last_page' => $agerangesArray['last_page'],
            'next_page_url' => $agerangesArray['next_page_url'],
            'prev_page_url' => $agerangesArray['prev_page_url'],
            'from' => $agerangesArray['from'],
            'to' => $agerangesArray['to'],
            'data' => array_map([$this, 'transformList'], $agerangesArray['data'])
        ];
    }
}
