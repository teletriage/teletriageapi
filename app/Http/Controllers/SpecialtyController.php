<?php

namespace App\Http\Controllers;

use App\Specialty;
use App\Util\Constants;
use Illuminate\Http\Request;

class SpecialtyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;

        $specialties = Specialty::where("name","LIKE","%{$request->input('query')}%")
                                ->orderBy('name', 'asc')
                                ->paginate($limit);

        $specialties->appends(array(
            'limit' => $limit
        ));

        return response()->json($this->transformCollection($specialties), Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $specialty = Specialty::find($id);
        if(!$specialty){
            return response()->json(['error' => ['message' => 'Espacialidad no encontrada']], Constants::NOT_FOUND_STATUS);
        }
        return response()->json(['data' => $this->transformSingle($specialty)], Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $specialty
     * @return array
     */
    private function transformList($specialty){
        return [
            'id' => $specialty['id'],
            'name' => $specialty['name'],
            'code' => $specialty['code']
        ];
    }

    /**
     * @param $locality
     * @return array
     */
    private function transformSingle($specialty){
        return [
            'id' => $specialty['id'],
            'name' => $specialty['name'],
            'code' => $specialty['code']
        ];
    }

    /**
     * @param $ageranges
     * @return array
     */
    private function transformCollection($specialties){
        $specialtiesArray = $specialties->toArray();
        return [
            'total' => $specialtiesArray['total'],
            'per_page' => intval($specialtiesArray['per_page']),
            'current_page' => $specialtiesArray['current_page'],
            'last_page' => $specialtiesArray['last_page'],
            'next_page_url' => $specialtiesArray['next_page_url'],
            'prev_page_url' => $specialtiesArray['prev_page_url'],
            'from' => $specialtiesArray['from'],
            'to' => $specialtiesArray['to'],
            'data' => array_map([$this, 'transformList'], $specialtiesArray['data'])
        ];
    }
}
