<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DocumentType;
use App\Util\Constants;


class DocumentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;

        $documentTypes= DocumentType::where("name","LIKE","%{$request->input('query')}%")->paginate($limit);

        $documentTypes->appends(array(
            'limit' => $limit
        ));

        return response()->json($this->transformCollection($documentTypes), Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $documentType = new DocumentType();

        $documentType->description = $request->description;
        $documentType->name = $request->name;
        $documentType->is_active = $request->is_active;

        $documentType->save();

        return response()->json([
            'message' => 'Successfully created Document Type'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $documentType
     * @return array
     */
    private function transformList($documentType){
        return [
            'id' => $documentType['id'],
            'name' => $documentType['name'],
            'description' => $documentType['description']
        ];
    }

    /**
     * @param $documentType
     * @return array
     */
    private function transformSingle($documentType){
        return [
            'id' => $documentType['id'],
            'name' => $documentType['name'],
            'description' => $documentType['description'],
        ];
    }

    /**
     * @param $documentType
     * @return array
     */
    private function transformCollection($documentTypes){
        $documentTypesArray = $documentTypes->toArray();
        return [
            'total' => $documentTypesArray['total'],
            'per_page' => intval($documentTypesArray['per_page']),
            'current_page' => $documentTypesArray['current_page'],
            'last_page' => $documentTypesArray['last_page'],
            'next_page_url' => $documentTypesArray['next_page_url'],
            'prev_page_url' => $documentTypesArray['prev_page_url'],
            'from' => $documentTypesArray['from'],
            'to' => $documentTypesArray['to'],
            'data' => array_map([$this, 'transformList'], $documentTypesArray['data'])
        ];
    }
}
