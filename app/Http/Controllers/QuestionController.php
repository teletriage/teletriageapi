<?php

namespace App\Http\Controllers;

use App\Question;
use App\Util\Constants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $questions = Question::with('questionCategory', 'ageRange', 'resultAction')->where('is_active', 1)->paginate($limit);
        $questions->appends(array(
            'limit' => $limit
        ));
        return response()->json($this->transformCollection($questions), Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $question = Question::find($id);
        if(!$question){
            return response()->json(['error' => ['message' => 'Pregunta no encontrada']], Constants::NOT_FOUND_STATUS);
        }
        return response()->json(['data' => $this->transformSingle($question)], Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $question
     * @return array
     */
    private function transformList($question){
        return [
            'id' => $question['id'],
            'body' => $question['body'],
            'answer' => $question['answer'],
            'is_active' => $question['is_active'],
            'question_category' => $this->transformQuestionCategory($question['question_category']),
            'age_range' => $this->transformAgeRange($question['age_range']),
            'result_action' => $this->transformResultAction($question['result_action'])
        ];
    }

    /**
     * @param $question
     * @return array
     */
    private function transformSingle($question){
        return [
            'id' => $question['id'],
            'body' => $question['body'],
            'answer' => $question['answer'],
            'is_active' => $question['is_active'],
            'question_category' => array_map([$this, 'transformQuestionCategory'], $question['question_category']->toArray()),
            'age_range' => array_map([$this, 'transformAgeRange'], $question['age_range']->toArray()),
            'result_action' => array_map([$this, 'transformResultAction'], $question['result_action']->toArray())
        ];
    }

    /**
     * @param $category
     * @return array
     */
    private function transformQuestionCategory($category){
        return [
            'id' => $category['id'],
            'title' => $category['title'],
            'description' => $category['description'],
            'is_active' => $category['is_active']
        ];
    }

    /**
     * @param $range
     * @return array
     */
    private function transformAgeRange($range){
        return [
            'id' => $range['id'],
            'title' => $range['title'],
            'description' => $range['description'],
            'is_active' => $range['is_active']
        ];
    }

    private function transformResultAction($resultAction){
        return [
            'id' => $resultAction['id'],
            'text' => $resultAction['text'],
            'level' => $resultAction['level'],
            'is_active' => $resultAction['is_active']
        ];
    }

    /**
     * @param $questions
     * @return array
     */
    private function transformCollection($questions){
        $questionsArray = $questions->toArray();
        return [
            'total' => $questionsArray['total'],
            'per_page' => intval($questionsArray['per_page']),
            'current_page' => $questionsArray['current_page'],
            'last_page' => $questionsArray['last_page'],
            'next_page_url' => $questionsArray['next_page_url'],
            'prev_page_url' => $questionsArray['prev_page_url'],
            'from' => $questionsArray['from'],
            'to' => $questionsArray['to'],
            'data' => array_map([$this, 'transformList'], $questionsArray['data'])
        ];
    }
}
