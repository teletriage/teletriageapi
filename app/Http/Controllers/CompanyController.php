<?php

namespace App\Http\Controllers;

use App\Company;
use App\Specialty;
use App\Util\Constants;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;

        $companies = Company::where("name","LIKE","%{$request->input('query')}%")
                        ->orderBy('name', 'asc')
                        ->paginate($limit);

        $companies->appends(array(
            'limit' => $limit
        ));

        return response()->json($this->transformCollection($companies), Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $company = Company::find($id);
        if(!$company){
            return response()->json(['error' => ['message' => 'Ips/Eps no encontrada']], Constants::NOT_FOUND_STATUS);
        }
        return response()->json(['data' => $this->transformSingle($company)], Constants::SUCCESS_STATUS);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $specialty
     * @return array
     */
    private function transformList($company){
        return [
            'id' => $company['id'],
            'name' => $company['name'],
            'entity_type' => $company['entity_type'],
            'entity_code' => $company['entity_code']
        ];
    }

    /**
     * @param $locality
     * @return array
     */
    private function transformSingle($company){
        return [
            'id' => $company['id'],
            'name' => $company['name'],
            'entity_type' => $company['entity_type'],
            'entity_code' => $company['entity_code']
        ];
    }

    /**
     * @param $ageranges
     * @return array
     */
    private function transformCollection($companies){
        $companiesArray = $companies->toArray();
        return [
            'total' => $companiesArray['total'],
            'per_page' => intval($companiesArray['per_page']),
            'current_page' => $companiesArray['current_page'],
            'last_page' => $companiesArray['last_page'],
            'next_page_url' => $companiesArray['next_page_url'],
            'prev_page_url' => $companiesArray['prev_page_url'],
            'from' => $companiesArray['from'],
            'to' => $companiesArray['to'],
            'data' => array_map([$this, 'transformList'], $companiesArray['data'])
        ];
    }
}
