<?php

namespace App\Http\Controllers;

use App\User;
use App\UserAction;
use App\UserLog;
use App\Util\Constants;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Vassar\Entities\Configuration;

class AuthController extends Controller
{
    /**
     * Metodo para registrar usuarios
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function signup(Request $request)
    {
        $request->validate([
            'name'              => 'required|string',
            'email'             => 'required|string|email|unique:users',
            'password'          => 'required|string|confirmed',
            'last_name'         => 'required|string',
            'document_type_id'  => 'required|integer',
            'document'          => 'required|string',
            'medical_licence'   => 'required|string',
            'company_id'        => 'required|integer',
            'speciality_id'     => 'required|integer',

        ]);
        $user = new User([
            'name'              => $request->name,
            'email'             => $request->email,
            'password'          => bcrypt($request->password),
            'last_name'         => $request->last_name,
            'document_type_id'  => $request->document_type_id,
            'document'          => $request->document,
            'medical_licence'   => $request->medical_licence,
            'company_id'        => $request->company_id,
            'speciality_id'     => $request->speciality_id,
        ]);
        $user->save();
        $user->assignRole(Constants::DOCTOR_ROLE);
        return response()->json(['message' => 'Usuario creado con éxito!'], Constants::CREATED_STATUS);
    }

    /**
     * Metodo para autenticar
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $request->validate([
            'email'       => 'required|string|email',
            'password'    => 'required|string',
            'remember_me' => 'boolean',
        ]);
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            return response()->json(['message' => 'No autorizado'], Constants::UNAUTHORIZED_STATUS);
        }

        // Inicio log de incio de sesión
        $user = User::where('email', $request['email'])->first();
        $loginAction = UserAction::where('description', 'Inicio Sesión')->first();
        $userLog = new UserLog;

        $userLog->user_id = $user->id;
        $userLog->user_action_id = $loginAction->id;

        $userLog->save();
        // Fin log de incio de sesión

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type'   => 'Bearer',
            'expires_at'   => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
        ], Constants::SUCCESS_STATUS);
    }

    /**
     * Metodo para salir
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        // Inicio log de cierre de sesión
        $loginAction = UserAction::where('description', 'Cierre Sesión')->first();
        $userLog = new UserLog;

        $userLog->user_id = $request->user()->id;
        $userLog->user_action_id = $loginAction->id;

        $userLog->save();
        // Fin log de cierre de sesión
        
        $request->user()->token()->revoke();
        return response()->json(['message' => 'Desconectado exitosamente']);
    }

    /**
     * Metodo para obtener al usuario autenticado
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function userResetPassword(Request $request){
        try{
            $user = User::where('email', $request['email'])->first();
            if($user != null){
                $new_clear_password = Str::random(12);
                $hashed_random_password = Hash::make($new_clear_password);

                $user->password = $hashed_random_password;
                $user->save();
                $data = ['new_password' => $new_clear_password, 'user' => $user];
                try{
                    Mail::send('emails.password', $data, function ($message) use ($user) {
                        $message->from('no-reply@vassar.com.co', "Teletriage");
                        $message->to(trim($user->email));
                        $message->subject("Teletriage" . 'Recuperación de contraseña TELETRIAGE');
                    });
                    return response()->json(['status' => "success", 'message' => 
                        'Alguien ha solicitado un restablecimiento de la contraseña para la siguiente cuenta: '+ $request['email']], 
                        200, 
                        array('Content-Type' => 'application/javascript'), 
                        JSON_NUMERIC_CHECK)->setCallback(Input::get('jsoncallback'));
                }catch (Exception $exc) {
                    Log::info("Error enviando contraseña nueva de usuario:" . $request['email'] . " contraseña: " . $new_clear_password . " Error: " . $exc->getMessage());
                    return response()->json(['status' => "fail", 'message' => 'Se produjo un error generando la nueva contrase&ntilde;a. Contacte al administrador.'], 500, array('Content-Type' => 'application/javascript'), JSON_NUMERIC_CHECK)->setCallback(Input::get('jsoncallback'));
                }
            }else{
                return response()->json(['status' => "fail", 'message' => 'No hemos podido encontrar su cuenta con esa informaci&oacute;n.'], 404, array('Content-Type' => 'application/javascript'), JSON_NUMERIC_CHECK)->setCallback(Input::get('jsoncallback'));
            }
        }catch (ModelNotFoundException $e){
            return response()->json(array('error' => $e->getMessage()));
        }
    }

    public function userChangePassword(Request $request){
        try{
            if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
                $user = User::where('email', $request['email'])->first();
                Log::info("Usuario:" . $request['email'] . " User Data: " . json_encode($user));

                if($user != null && $request['password'] != null && $request['new_password'] != null){
                    $hashed_random_password = Hash::make($request['new_password']);
                    $user->password = $hashed_random_password;
                    $user->save();
                    return response()->json(['status' => "success", 'message' => 'Se ha cambiado la contrase&ntilde;a correctamente.'], 200, array('Content-Type' => 'application/javascript'), JSON_NUMERIC_CHECK)->setCallback(Input::get('jsoncallback'));
                }else{
                    return response()->json(['status' => "fail", 'message' => 'Su contrase&ntilde;a no se ha cambiado.'], 200, array('Content-Type' => 'application/javascript'), JSON_NUMERIC_CHECK)->setCallback(Input::get('jsoncallback'));
                }
            }else{
                return response()->json(['status' => "fail", 'message' => 'La contrase&ntilde;a anterior no es la correcta, su contrase&ntilde;a no se ha cambiado.'], 200, array('Content-Type' => 'application/javascript'), JSON_NUMERIC_CHECK)->setCallback(Input::get('jsoncallback'));
            }
        }catch (ModelNotFoundException $e){
            return response()->json(array('error' => $e->getMessage()));
        }
    }
}
