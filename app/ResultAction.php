<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultAction extends Model
{

   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text', 'level', 'is_active',
   ];

   // TODO check why I have to do this...
   // I got a "Illuminate/Database/QueryException with message 'SQLSTATE[HY000]:
   // General error: 1364 Field 'is_active' doesn't have a default value"
   // I tried several updates and nothing works...
   protected $attributes = [
      'is_active' => true
   ];
}
