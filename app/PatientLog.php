<?php

namespace App;

use App\Http\Controllers\AgeRangeController;
use Dotenv\Result\Result;
use Illuminate\Database\Eloquent\Model;

class PatientLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'municipality_id',
        'locality_id',
        'age_range_id',
        'age',
        'symptoms_origin_date',
        'gender',
        'company_id',
        'result_action_id'
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function municipality()
    {
        return $this->belongsTo(Municipality::class, 'municipality_id');
    }

    public function locality()
    {
        return $this->belongsTo(Locality::class, 'locality_id');
    }

    public function ageRange()
    {
        return $this->belongsTo(AgeRange::class, 'age_range_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function resultAction()
    {
        return $this->belongsTo(ResultAction::class, 'result_action_id');
    }
}
