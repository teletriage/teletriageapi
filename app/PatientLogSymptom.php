<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientLogSymptom extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'patient_log_id',
        'question_id',
    ];

    public function patientLog()
    {
        return $this->belongsTo(PatientLog::class, 'patient_log_id');
    }

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }
}
