<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'body', 'answer', 'is_active',
        'question_category_id', 'age_range_id', 'result_action_id',
   ];

    public function questionCategory(){
        return $this->belongsTo(QuestionCategory::class, 'question_category_id');
    }

    public function ageRange(){
        return $this->belongsTo(AgeRange::class, 'age_range_id');
    }

    public function resultAction(){
        return $this->belongsTo(ResultAction::class,'result_action_id');
    }

}
