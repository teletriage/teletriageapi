<?php

namespace App\Util;

class Constants {

    const SUCCESS_STATUS = 200;

    const CREATED_STATUS = 201;

    const UNAUTHORIZED_STATUS = 401;

    const FORBIDDEN_STATUS = 403;

    const NOT_FOUND_STATUS = 404;

    const ADMIN_ROLE = "Admin";

    const DOCTOR_ROLE = "Doctor";

    const IS_ADMIN_MIDD = "isAdmin";

    const RESOURCE_MOD_MIDD = "resourceModification";

}
