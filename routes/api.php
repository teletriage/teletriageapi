<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    /* TODO cambiar ruta para centralizar en usuarios */
    Route::post('signup', 'AuthController@signup');

    Route::post('resetPassword', 'AuthController@userResetPassword');
    Route::post('changePassword', 'AuthController@userChangePassword');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::middleware('auth:api')->group( function () {
    Route::resource('users', 'UserController');
    Route::resource('agerange', 'AgeRangeController');
    Route::resource('questioncategory', 'QuestionCategoryController');
    Route::resource('question', 'QuestionController');
    Route::resource('resultaction', 'ResultActionController');
    Route::get('roles', 'PermissionController@role_list');
    Route::post('roles', 'PermissionController@role_store');
    Route::get('permissions', 'PermissionController@permission_list');
    Route::post('permissions', 'PermissionController@permission_store');
    Route::post('rolepermissions', 'PermissionController@role_has_permissions');
    Route::post('assignuserrole', 'PermissionController@assign_user_to_role');
});

Route::resource('specialties', 'SpecialtyController');
Route::resource('companies', 'CompanyController');
Route::resource('localities', 'LocalityController');
Route::resource('municipalities', 'MunicipalityController');
Route::resource('userLogs', 'UserLogController');
Route::resource('patientLogs', 'PatientLogController');
Route::resource('patientLogSymptom', 'PatientLogSymptomController');
Route::resource('legalInfo', 'LegalInfoController');
Route::resource('documentTypes', 'DocumentTypeController');
Route::resource('userAction', 'UserActionController');

Route::fallback(function(){
    return response()->json(['message' => 'No tiene la autorización requerida'], \App\Util\Constants::FORBIDDEN_STATUS);
})->name('api.fallback.403');

Route::fallback(function(){
    return response()->json(['message' => 'No encontrado'], \App\Util\Constants::NOT_FOUND_STATUS);
})->name('api.fallback.404');


