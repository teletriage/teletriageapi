FROM gergom/alpine-php743-nginx17-composer19:latest

RUN echo "America/Bogota" > /etc/timezone
ENV TZ=America/Bogota
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apk update && apk add --no-cache libxml2-dev gcc make autoconf libc-dev openssh-server autossh libpng-dev libmcrypt-dev libzip-dev oniguruma-dev && \
    docker-php-ext-install soap && docker-php-ext-install zip && docker-php-ext-install mbstring && docker-php-ext-install gd && docker-php-ext-install mysqli pdo pdo_mysql && \
    docker-php-ext-enable pdo_mysql && docker-php-ext-enable mysqli

RUN pecl install redis && docker-php-ext-enable redis
COPY docker/php.ini "$PHP_INI_DIR/php.ini"

WORKDIR /var/www/html/symfony

ADD ./composer.json /var/www/html/symfony/composer.json

ADD ./composer.lock /var/www/html/symfony/composer.lock

COPY . /var/www/html/symfony/

WORKDIR /var/www/html/symfony

ARG APP_ENVIRONMENT
ENV APP_ENVIRONMENT $APP_ENVIRONMENT

RUN /usr/local/bin/composer install --no-dev --optimize-autoloader --prefer-dist

RUN chmod 777 -R *

COPY docker/nginx/nginx.conf /etc/nginx/nginx.conf

ADD docker/nginx/sites-enabled /etc/nginx/conf.d

RUN rm /etc/nginx/conf.d/default.conf

RUN rm /usr/local/etc/php-fpm.d/www.conf

COPY docker/www.conf /usr/local/etc/php-fpm.d/www.conf

RUN rm /var/www/html/symfony/.env
COPY ".env.$APP_ENVIRONMENT" /var/www/html/symfony/.env

WORKDIR /var/www/html/

COPY docker/docker-entrypoint.sh docker-entrypoint.sh

RUN chmod 555 docker-entrypoint.sh

CMD ./docker-entrypoint.sh
