<?php

use Illuminate\Database\Seeder;

class UserActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_action = new App\UserAction;
        $user_action -> title = 'Inicio Sesión';
        $user_action -> description = 'Inicio Sesión';
        $user_action -> is_active = 1;
        $user_action -> save();

        $user_action = new App\UserAction;
        $user_action -> title = 'Cierre Sesión';
        $user_action -> description = 'Cierre Sesión';
        $user_action -> is_active = 1;
        $user_action -> save();

        $user_action = new App\UserAction;
        $user_action -> title = 'Inicio Diagnostico';
        $user_action -> description = 'Inicio Diagnostico';
        $user_action -> is_active = 1;
        $user_action -> save();

        $user_action = new App\UserAction;
        $user_action -> title = 'Finalización de Diagnostico';
        $user_action -> description = 'Finalización de Diagnostico';
        $user_action -> is_active = 1;
        $user_action -> save();

        $this->command->info('User Actions seeded!');
    }
}
