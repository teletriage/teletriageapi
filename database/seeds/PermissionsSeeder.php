<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder
{
    /**
     * Create the initial roles and permissions.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'Edit']);
        Permission::create(['name' => 'Delete']);
        Permission::create(['name' => 'Create']);
        Permission::create(['name' => 'Read']);

        $role1 = Role::create(['name' => 'Doctor']);
        $role1->givePermissionTo('Create');
        $role1->givePermissionTo('Edit');
        $role1->givePermissionTo('Read');

        $role2 = Role::create(['name' => 'Admin']);
        $role2->givePermissionTo('Edit');
        $role2->givePermissionTo('Delete');
        $role2->givePermissionTo('Create');
        $role2->givePermissionTo('Read');
    }
}
