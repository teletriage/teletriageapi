<?php

use App\Company;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $handle = fopen('seed_information/companies.csv', "r");
        $header = true;

        while ($csvLine = fgetcsv($handle, 1000, ",")) {
            if ($header) {
                $header = false;
                continue;
            }

            $entity_type = ($this->sanitize_string($csvLine[0]));
            $entity_code = ($this->sanitize_string($csvLine[1]));
            $name = ($this->sanitize_string($csvLine[2]));

            $company = new Company();
            $company->name = $name;
            $company->entity_type = $entity_type;
            $company->entity_code = $entity_code;
            $company->save();
            echo(".");
        }
        $this->command->info('DB seeded!');
    }

    /**
     * removes spaces at the end and at the beginning, also removes Newline
     * character.
     *
     * @return string
     */
    function sanitize_string($string) {
        return trim(preg_replace( "/\r|\n/", "", $string ), ' ');
    }
}
