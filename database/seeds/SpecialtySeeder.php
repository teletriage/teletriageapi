<?php

use App\Specialty;
use Illuminate\Database\Seeder;

class SpecialtySeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $handle = fopen('seed_information/specialties.csv', "r");
        $header = true;

        while ($csvLine = fgetcsv($handle, 1000, ",")) {
            if ($header) {
                $header = false;
                continue;
            }

            $code = ($this->sanitize_string($csvLine[0]));
            $name = ($this->sanitize_string($csvLine[1]));

            $specialty = new Specialty();
            $specialty->name = $name;
            $specialty->code = $code;
            $specialty->save();
            echo(".");
        }
        $this->command->info('DB seeded!');
    }

    /**
     * removes spaces at the end and at the beginning, also removes Newline
     * character.
     *
     * @return string
     */
    function sanitize_string($string) {
        return trim(preg_replace( "/\r|\n/", "", $string ), ' ');
    }
}
