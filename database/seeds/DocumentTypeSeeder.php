<?php

use Illuminate\Database\Seeder;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $document_type = new App\DocumentType;
        $document_type -> name = 'Cédula de Ciudadanía';
        $document_type -> description = 'Cédula de Ciudadanía';
        $document_type -> is_active = 1;
        $document_type -> save();

        $document_type = new App\DocumentType;
        $document_type -> name = 'Tarjeta de Identidad';
        $document_type -> description = 'Tarjeta de Identidad';
        $document_type -> is_active = 1;
        $document_type -> save();

        $document_type = new App\DocumentType;
        $document_type -> name = 'Cédula de Extranjería';
        $document_type -> description = 'Cédula de Extranjería';
        $document_type -> is_active = 1;
        $document_type -> save();

        $document_type = new App\DocumentType;
        $document_type -> name = 'Pasaporte';
        $document_type -> description = 'Pasaporte';
        $document_type -> is_active = 1;
        $document_type -> save();

        $document_type = new App\DocumentType;
        $document_type -> name = 'Permiso Especial de Permanencia';
        $document_type -> description = 'Permiso Especial de Permanencia';
        $document_type -> is_active = 1;
        $document_type -> save();

        $document_type = new App\DocumentType;
        $document_type -> name = 'Documento Nacional de Identificación';
        $document_type -> description = 'Documento Nacional de Identificación';
        $document_type -> is_active = 1;
        $document_type -> save();
    }
}
