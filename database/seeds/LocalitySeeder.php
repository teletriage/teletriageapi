<?php

use App\Locality;
use Illuminate\Database\Seeder;

class LocalitySeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $handle = fopen('seed_information/localities.csv', "r");
        $header = true;

        while ($csvLine = fgetcsv($handle, 1000, ",")) {
            if ($header) {
                $header = false;
                continue;
            }

            $code = ($this->sanitize_string($csvLine[0]));
            $name = ($this->sanitize_string($csvLine[1]));

            $locality = new Locality();
            $locality->name = $name;
            $locality->code = $code;
            $locality->save();
            echo(".");
        }
        $this->command->info('DB seeded!');
    }

    /**
     * removes spaces at the end and at the beginning, also removes Newline
     * character.
     *
     * @return string
     */
    function sanitize_string($string) {
        return trim(preg_replace( "/\r|\n/", "", $string ), ' ');
    }
}
