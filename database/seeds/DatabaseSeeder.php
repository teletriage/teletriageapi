<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

class DatabaseSeeder extends Seeder
{
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run()
  {
    $handle = fopen('seed_information/seed.csv', "r");
    $header = true;
    $categories = array();
    $age_ranges = array();
    $user_actions = array();

    while ($csvLine = fgetcsv($handle, 1000, ",")) {
      if ($header) {
        $header = false;
        continue;
      }

      $category = ($this -> sanitize_string($csvLine[0]));
      $age_range_label = ($this -> sanitize_string($csvLine[1]));
      $action = ($this -> sanitize_string($csvLine[2]));
      $action_level = ($this -> sanitize_string($csvLine[3]));
      $question_body = ($this -> sanitize_string($csvLine[4]));
      $result = ($this -> sanitize_string($csvLine[5]));
      $active = (filter_var(($this -> sanitize_string($csvLine[6])),FILTER_VALIDATE_BOOLEAN));

      $question_category = App\QuestionCategory::firstOrCreate(
        ['title' => $category],
        ['description' => '--'],
        ['is_active' => $active]
      );
      $age_range = App\AgeRange::firstOrCreate(
        ['title' => $age_range_label],
        ['description' => '--'],
        ['is_active' => true]
      );
      $result_action = App\ResultAction::firstOrCreate(
        ['text' => $action],
        ['level' => (int)$action_level],
        ['is_active' => true]
      );
      $question = new App\Question;
      $question -> body = $question_body;
      $question -> answer = $result;
      $question -> is_active = $active;
      $question -> question_category_id = $question_category['id'];
      $question -> age_range_id = $age_range['id'];
      $question -> result_action_id = $result_action['id'];
      $question -> save();
      echo(".");
    }
    $this->command->info('DB seeded!');
  }

  /**
   * removes spaces at the end and at the beginning, also removes Newline
   * character.
   *
   * @return string
   */
  function sanitize_string($string) {
    return trim(preg_replace( "/\r|\n/", "", $string ), ' ');
  }
}
