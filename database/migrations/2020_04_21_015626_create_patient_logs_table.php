<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_logs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->bigInteger('municipality_id')->unsigned();
            $table->foreign('municipality_id')
                ->references('id')->on('municipalities')
                ->onDelete('cascade');
            $table->bigInteger('locality_id')->unsigned();
            $table->bigInteger('age_range_id')->unsigned();
            $table->foreign('age_range_id')
                ->references('id')->on('age_ranges')
                ->onDelete('cascade');
            $table->integer('age');
            $table->date('symptoms_origin_date');
            $table->Integer('gender');
            $table->bigInteger('company_id')->unsigned();
            $table->foreign('company_id')
                ->references('id')->on('companies')
                ->onDelete('cascade');
            $table->bigInteger('result_action_id')->unsigned();
            $table->foreign('result_action_id')
                ->references('id')->on('result_actions')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_logs');
    }
}
