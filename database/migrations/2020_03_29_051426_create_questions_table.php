<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->string('body');
            $table->longText('answer');
            $table->boolean('is_active');
            $table->bigInteger('question_category_id')->unsigned();
            $table->foreign('question_category_id')
                ->references('id')->on('question_categories')
                ->onDelete('cascade');
            $table->bigInteger('age_range_id')->unsigned();
            $table->foreign('age_range_id')
                ->references('id')->on('age_ranges')
                ->onDelete('cascade');
            $table->bigInteger('result_action_id')->unsigned();
            $table->foreign('result_action_id')
                ->references('id')->on('result_actions')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
