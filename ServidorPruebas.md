#  Creación de un servidor de Pruebas

[[_TOC_]]

## Requisitos

*  Software para maquinas virutales como [VirtualBox](https://www.virtualbox.org/)
*  Cliente ssh como [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
*  Descargar el SO de servidor [Ubuntu server 18.04](https://ubuntu.com/download/server)

## Paso a paso

1.  Instalar el servidor siguiendo los pasos del video
2.  Verficar la ip del servidor
	```console
	ip a
    ```
3. Conectarse al servidor por ssh
    ```console
	ssh server@ip
    ```
    
4. Instalamos Docker
	```console
    sudo apt install docker.io
    ```
    
5. Instalamos un contenedor para nuestra base de datos
    ```console
    mkdir mariadb
    
    cd mariadb
    
    sudo docker run --name mariadb -v $PWD:/var/lib/mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mariadb:latest
    ```

6. Nos conectamos con un cliente y generamos la base de datos de pruebas


7. Subimos un directorio
    ```console    
    cd ..
    ```

8. Clonamos el back end
    ```console
    git clone https://gitlab.com/teletriage/teletriageapi.git
    ```
9. Dar permisos a la carpeta de storage
    ```console
    chmod 777 -R storage
    ```
10. Seguimos la guia de [instalación](https://gitlab.com/teletriage/teletriageapi/-/blob/master/README.md)
11. Ejecutamos el contenedor

    ```console
    sudo docker run --name=teletriage-api-local --env APP_ENVIRONMENT=local --rm -v $PWD:/var/www/html/symfony/ -p 80:80/tcp -d teletriage-api-local
    ```

12. Para Ver las rutas de la api
    ```console
    php artisan route:list
    ```
